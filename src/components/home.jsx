import React, { useState, useEffect } from 'react';
import { __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from 'react-dom';
import Category from './categories/category';
import SearchBar from './marketplace/search-bar/searchBar';

import Product from './marketplace/product';


function Home() {
    const categories = [
        { title: "All", image: "./img/icon4.png" },
        { title: "Ferrous Metals", image: "./img/icon1.png" },
        { title: "Non Ferrous", image: "./img/icon2.png" },
        { title: "Carton", image: "./img/icon3.png" },
        { title: "Paper", image: "./img/icon4.png" },
        { title: "WEEE", image: "./img/icon2.png" },
        { title: "Electronics", image: "./img/icon1.png" },
        { title: "Plastics", image: "./img/icon3.png" },
    ];
    const logos = [
        { image: "./img/microsoft.png" },
        { image: "./img/airbnb.png" },
        { image: "./img/netflix.png" },
        { image: "./img/google.png" },
        { image: "./img/netflix.png" },
    ];

    const products = [
        { type: "sell", title: "Bulk Plastic", description: "plastic in bulk, not handled", price: 38, currency: "USD", location: "Jounieh", image: "./img/plastic.jpg" },
        { type: "buy", title: "Premium Plastic Bottle Cap", description: "premium plastic bottle caps shredded and cleaned from liquids", price: 128, currency: "EUR", location: "Beirut", image: "./img/bottlecap.jpg" },
        { type: "sell", title: "Paper", description: "these papers are collected and cleaned from any plastic, tape, or steel", price: 10000, currency: "LBP", location: "Tripoly", image: "./img/paper.jpg" },
    ];

    const searchBar =
    {
        image: "./img/search.png",
        placeholder: "Businesses, Services, Stores, Shops ...",
        options: ["ALL", "BUY", "SELL"],
        search: "Search",
    };
    useEffect(() => {

    }, []);

    return (
        <>
            <div className="bg-gray-50 py-10 select-none">
                <div className="px-2 md:px-7 py-5 grid grid-cols-8 gap-x-4 gap-y-3 bg-white mx-10 rounded">
                    <div className="col-span-8">
                        <div className="text-left text-green-900">
                            Filter by type
                        </div>
                    </div>
                    {categories.map((category, i) => {
                        // selected logic is based on the iterator for now  
                        return <div className={`cursor-pointer py-4 category rounded lg:col-span-1 md:col-span-2 col-span-4 ` + (i == 0 ? " bg-green-700 text-gray-200" : ` bg-gray-200  duration-300 hover:bg-green-500 text-green-900 hover:text-gray-200`)}>
                            <Category category={category} />
                        </div>
                    })}
                </div>
            </div>

            <div className="grid-cols-12 grid gap-4 px-2 md:px-7 py-5 mx-10 select-none">
                {products.map((product, i) => {
                    return <div className="lg:col-span-4 md:col-span-6 col-span-12">
                        <Product product={product} />
                    </div>
                })}
            </div>

            <div className="bg-gray-50 py-10 select-none">
                <div className="px-2 md:px-7 py-5 justify-center grid grid-cols-6 gap-x-4 gap-y-5 bg-white mx-10 rounded">
                    <div className="col-span-6 lg:col-span-1">
                        <div className="lg:text-left text-green-900">
                            Trusted By Industry Experts
                        </div>
                    </div>
                    {logos.map((logo, i) => {
                        // selected logic is based on the iterator for now  
                        return <div className="lg:col-span-1 md:col-span-2 col-span-3 middle">
                            <img className="max-h-8" src={logo.image} alt="logo" />
                        </div>
                    })}
                </div>
            </div>
            <div className="text-left bg-gray-50 py-10 px-2 md:px-7 mx-10 select-none">
                <div className="text-xl text-black">
                    Make Waste a Resource
                </div>
                <div className="text-gray-400 text-md">
                    Buy, Sell or Give recycled materials
                </div>
            </div>

            <div className="select-none grid grid-cols-4 text-left gap-y-4 bg-gray-50 py-10 px-2 md:px-7 mx-10 ">
                <div className="md:col-span-2 col-span-4 flex items-center">
                    <div className="pr-5">
                        <img className="max-h-4" src="./img/check.png" alt="check" />
                    </div>
                    <div className="text-xl text-black">
                        Make Waste a Resource
                    </div>
                </div>
                <div className="md:col-span-2 col-span-4 flex items-center">
                    <span className="pr-5">
                        <img className="max-h-4" src="./img/check.png" alt="check" />
                    </span>
                    <span className="text-xl text-black">
                        Make Waste a Resource
                    </span>
                </div>
                <div className="md:col-span-2 col-span-4 flex items-center">
                    <span className="pr-5">
                        <img className="max-h-4" src="./img/check.png" alt="check" />
                    </span>
                    <span className="text-xl text-black">
                        Make Waste a Resource
                    </span>
                </div>
                <div className="md:col-span-2 col-span-4 flex items-center">
                    <span className="pr-5">
                        <img className="max-h-4" src="./img/check.png" alt="check" />
                    </span>
                    <span className="text-xl text-black">
                        Make Waste a Resource
                    </span>
                </div>
            </div>

            <div className="py-10 select-none">
                <SearchBar searchbar={searchBar} />
            </div>


        </>
    );
}

export default Home;