import React, { useState, useEffect } from 'react';
import CounterCard from './landingPage/counter-card';
import Section2 from './landingPage/section2';
import Section3 from './landingPage/section3';

import RecycleCard from './recyclable/recyclableCard';
import Swiper from './howToRecycle/howToRecycleSlider';
import RecycleSlider from './howToRecycle/howToRecycleSlider';

import Product from './marketplace/product';
import ProductDetails from './marketplace/product-details/productDetails';

function Home() {

    const [subtitles, setSubtitles] = useState([]);
    const [titles, setTitles] = useState([]);
    const [numbers, setNumbers] = useState([]);
    const [slides, setSlides] = useState([]);
    const [recycleCards, setRecycleCards] = useState([]);
    const [products, setProducts] = useState([]);

    useEffect(() => {
        let tempSubtitles = ['lorem 1 lorem 1 lorem 1 lorem 1 lorem 1', 'lorem 2 lorem 2 lorem 2 lorem 2 lorem 2', 'lorem 3 lorem 3 lorem 3 lorem 3 lorem 3'];
        let tempTitles = ['this is the card title 1', 'this is the card title 2', 'this is the card title 3'];
        let tempNumbers = [10000, 20000, 30000];
        let tempSlides = [
            { title: "this is the title 1", subtitle: "this is the subtitle 1", image: "https://recyclebc.ca/wp-content/uploads/2019/08/What-Happens-to-Recycling-Step-1-Recycle-BC-10-may-2018.png" },
            { title: "this is the title 2", subtitle: "this is the subtitle 2", image: "https://recyclebc.ca/wp-content/uploads/2019/08/What-Happens-to-Recycling-Step-1-Recycle-BC-10-may-2018.png" },
            { title: "this is the title 3", subtitle: "this is the subtitle 3", image: "https://recyclebc.ca/wp-content/uploads/2019/08/What-Happens-to-Recycling-Step-1-Recycle-BC-10-may-2018.png" },
            { title: "this is the title 4", subtitle: "this is the subtitle 4", image: "https://recyclebc.ca/wp-content/uploads/2019/08/What-Happens-to-Recycling-Step-1-Recycle-BC-10-may-2018.png" },
        ];
        let tempRecycleCards = [
            { description: "this is the description 1", confirmed: 1, image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
            { description: "this is the description 2", confirmed: 0, image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
            { description: "this is the description 3", confirmed: 0, image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
            { description: "this is the description 4", confirmed: 1, image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
        ];

        let tempProducts = [
            { description: "this is the description 1", type: "sell", title: "this is the title 1", description: "this is the description", price: 38, currency: "USD", location: "Jounieh", image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
            { description: "this is the description 1", type: "buy", title: "this is the title 2", description: "this is the description", price: 38, currency: "USD", location: "Jounieh", image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
            { description: "this is the description 1", type: "sell", title: "this is the title 3", description: "this is the description", price: 38, currency: "USD", location: "Jounieh", image: "https://www.corvallisoregon.gov/sites/default/files/styles/gallery500/public/imageattachments/publicworks/page/18073/pw-recycling.png?itok=Y8esemWv" },
        ];
        setSubtitles(tempSubtitles);
        setTitles(tempTitles);
        setNumbers(tempNumbers);
        setSlides(tempSlides);
        setRecycleCards(tempRecycleCards);
        setProducts(tempProducts);
    }, []);

    return (
        <div className="container py-5  my-5">
            <Section2 titles={titles} subtitles={subtitles} />
            <Section3 title="section 3 title" subtitle="section 3 subtitle" numbers={numbers} subtitles={subtitles} />

            <div className="row justify-content-center bg-grey">
                {recycleCards.map((card, i) => {
                    return <div className="my-4 col-md-4 col-sm-6 col-12">
                        <RecycleCard card={card} />
                    </div>
                })}
            </div>

            <div className="">
                <RecycleSlider slides={slides} />
            </div>

            <div className="grid-cols-12 grid gap-4">
                {products.map((product, i) => {
                    return <div className="lg:col-span-4 md:col-span-6 col-span-12">
                        <Product product={product} />
                    </div>
                })}
            </div>

            <div className="">
                <ProductDetails product={products[2]} />
            </div>


        </div>
    );
}

export default Home;