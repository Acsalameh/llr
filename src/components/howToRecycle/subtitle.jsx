import React from 'react';

function Subtitle(props) {

    return (
        <div className="subtitlef-14 color-grey">
            {props.subtitle}
        </div>
    );
}

export default Subtitle;