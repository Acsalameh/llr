import React from 'react';
import Title from './title';
import Subtitle from './subtitle';

function StepText(props) {

    return (
        <div className="step-text text-start h-100 middle py-md-0 py-5 px-md-0 px-4">
            <div className="pt-5 pt-md-0">
                <div className="step">
                    Step {props.counter + 1}
                </div>
                <Title title={props.title} />
                <Subtitle subtitle={props.subtitle} />
            </div>
        </div>
    );
}

export default StepText;