import React from 'react';
import StepImage from './stepImage';
import StepText from './stepText';
import Slider from "react-slick";

function RecycleSlider(props) {
    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1
    };
    return (
        <Slider {...settings}>
            {props.slides.map((slide, i) => {
                return <div className="recycle-slider py-5 my-5 container">
                    <div className="row">
                        <div className="offset-md-2 offset-0 col-md-10 col-12 position-relative">
                            <div className="step-img-pos">
                                <StepImage
                                    image={slide.image}
                                />
                            </div>
                            <StepText counter={i} title={slide.title} subtitle={slide.subtitle} />
                        </div>
                    </div>
                </div>
            })}
        </Slider>
    );
}

export default RecycleSlider;
