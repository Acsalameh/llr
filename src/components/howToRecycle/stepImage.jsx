import React from 'react';

function StepImage(props) {

    return (
        <div className="step-image">
            <img className="" src={props.image} alt="image" />
        </div>
    );
}

export default StepImage;