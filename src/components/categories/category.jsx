import React from 'react';

function Category(props) {

    return (

        <div className="">
            <div className="py-5 middle">
                <img src={props.category.image} alt="recycling icon" />
            </div>
            <div className="font-bold">
                {props.category.title}
            </div>
        </div>
    );
}

export default Category;