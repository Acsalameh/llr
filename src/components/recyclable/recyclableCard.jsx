import React from 'react';
import ConfirmedImage from './confirmedImage';
import RecycleImage from './recycleImage';

function RecycleCard(props) {

    return (
        <div className="container recycle-card py-4 bg-white">
            <div className="row">
                <div className="col-7">
                    <RecycleImage image={props.card.image} />
                </div>
                <div className="col-5">
                    <ConfirmedImage confirmed={props.card.confirmed} />
                </div>
                <div className="col-12 pt-5">
                    <div className="color-green">
                        {props.card.description}
                    </div>
                </div>
            </div>
        </div>
    );
}

export default RecycleCard;