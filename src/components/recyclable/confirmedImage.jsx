import React from 'react';

function ConfirmedImage(props) {

    return (
        <div className="confirmed-image">
            {props.confirmed == 1 ?
                <>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Checkmark_green.svg/1200px-Checkmark_green.svg.png" alt="green check" className="" />
                </>
                :
                <>
                    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5f/Red_X.svg/768px-Red_X.svg.png" alt="red X" className="" />
                </>
            }
        </div>
    );
}

export default ConfirmedImage;