import React from 'react';

function RecycleImage(props) {

    return (
        <div className="recycle-image">
            <img src={props.image} alt="" />
        </div>
    );
}

export default RecycleImage;