import React from 'react';
import CountUp from 'react-countup';

function Number(props) {

    return (
        <div className="number f-36">
            <strong>
                <CountUp end={props.number} />
            </strong>
        </div>
    );
}

export default Number;