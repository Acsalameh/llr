import React from 'react';

function Subtitle(props) {

    return (
            <div className="subtitle">
                {props.subtitle}
            </div>
    );
}

export default Subtitle;