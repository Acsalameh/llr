import React from 'react';
import Text from './card/text';
import Image from './card/image';

function Card(props) {

    return (
        <div className="row">
            <div className={props.i % 2 == 0? `col-lg-6 col-12 middle order-lg-0 order-0`:`col-lg-6 col-12 middle order-lg-1 order-0`}>
                <Text title={props.title} subtitle={props.subtitle} />
            </div>
            <div className={props.i % 2 == 0? `col-lg-6 col-12 middle order-lg-1 order-1`:`col-lg-6 col-12 middle order-lg-0 order-1`}>
                <Image image={props.image} />
            </div>
        </div>
    );
}

export default Card;