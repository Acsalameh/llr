import React from 'react';
import CounterCard from './counter-card';

function Section3(props) {

    return (
        <>
            <div className="row justify-content-center">
                <div className="col-12">
                    <div className="f-34">
                        <strong>
                            {props.title}
                        </strong>
                    </div>
                </div>
                <div className="col-12 pb-5">
                    <div className="color-grey">
                        {props.subtitle}
                    </div>
                </div>

                {props.numbers.map((element, i) => {
                    return <div className="col-lg-4 px-md-4 col-md-6 col-8 mb-5"> <CounterCard number={element} subtitle={props.subtitles[i]} /></div>
                })}

            </div>




        </>
    );
}

export default Section3;