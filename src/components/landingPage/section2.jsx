import React from 'react';
import Card from './card';

function Section2(props) {

    return (
        <>
            {props.subtitles.map((element, i) => {
                return <div className="py-4"> <Card i={i} subtitle={element} title={props.titles[i]} image="./logo192.png" /></div>
            })}
        </>
    );
}

export default Section2;