import React from 'react';
import Number from './counter-card/number';
import Subtitle from './counter-card/subtitle';

function CounterCard(props) {

    return (
        <div className="counter-card pointer border-grey br-25 middle row">
            <Number number={props.number} className="col-12" />
            <Subtitle subtitle={props.subtitle} className="col-12" />
        </div>
    );
}

export default CounterCard;