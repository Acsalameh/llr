import React from 'react';

function Title(props) {

    return (
        <div className="title f-34">
            <strong>
                {props.title}
            </strong>
        </div>
    );
}

export default Title;