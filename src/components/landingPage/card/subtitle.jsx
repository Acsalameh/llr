import React from 'react';

function Subtitle(props) {

    return (
        <div className="subtitle color-grey">
            {props.subtitle}
        </div>
    );
}

export default Subtitle;