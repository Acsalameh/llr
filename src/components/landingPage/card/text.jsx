import React from 'react';
import Title from './title';
import Subtitle from './subtitle';

function Text(props) {

    return (
        <div className="text">
            <Title title={props.title} />
            <Subtitle subtitle={props.subtitle} />
        </div>
    );
}

export default Text;