import React from 'react';

function Image(props) {

    return (

        <div className="image-wrapper-60">
            <div className={(props.product.type == "sell" ? "bg-red-500" : "bg-green-500") + " z-10 absolute px-4 py-3 top-3 md:-right-3 -right-1 uppercase text-white font-bold"}>
                {props.product.type}
            </div>
            <img  className="image-absolute" src={props.product.image} alt="image" />
        </div>
    );
}

export default Image;