import React from 'react';

function Price(props) {

    return (
        <div className="grid grid-cols-12 gap-4 justify-items-start">
            <div className="col-span-4">
                <div className={(props.product.type == "sell" ? "text-red-500" : "text-green-500") +" font-bold text-2xl"}>
                    {props.product.price + " "} {props.product.currency}
                </div>
            </div>
            <div className="col-span-8 middle">
                <div className="text-gray-500">
                    {props.product.description}
                </div>
            </div>
        </div>

    );
}

export default Price;