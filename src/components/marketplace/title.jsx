import React from 'react';

function Title(props) {

    return (
        <div className="font-bold capitalize text-lg">
            {props.product.title}
        </div>
    );
}

export default Title;