import React from 'react';

function ProductDetails(props) {

    function setMainImage(e) {
        let mainSrc = document.getElementById("main-image").getAttribute("src");
        document.getElementById("main-image").setAttribute("src", e.target.getAttribute("src"));
        e.target.setAttribute("src", mainSrc);
    }

    return (
        <div className="grid grid-cols-12 my-20 gap-4 justify-start product-details">

            <div className="md:col-span-5 col-span-12">
                <div className="grid grid-cols-12 gap-4 justify-start">
                    <div className="col-span-12">
                        <div className="overflow-hidden image-wrapper-100">
                            <img id="main-image" className="main-image" src={'https://recyclebc.ca/wp-content/uploads/2019/08/What-Happens-to-Recycling-Step-1-Recycle-BC-10-may-2018.png'} alt="image" />
                        </div>
                    </div>
                    <div className="col-span-3">
                        <div className="overflow-hidden image-wrapper-100">
                            {console.log(props)}
                            <img onClick={setMainImage} className="small-image cursor-pointer" src={props.product.image} alt="image" />
                        </div>
                    </div>
                    <div className="col-span-3">
                        <div className="overflow-hidden image-wrapper-100">
                            <img onClick={setMainImage} className="small-image cursor-pointer" src={props.product.image} alt="image" />
                        </div>
                    </div>
                    <div className="col-span-3">
                        <div className="overflow-hidden image-wrapper-100">
                            <img onClick={setMainImage} className="small-image cursor-pointer" src={props.product.image} alt="image" />
                        </div>
                    </div>
                    <div className="col-span-3">
                        <div className="overflow-hidden image-wrapper-100">
                            <img onClick={setMainImage} className="small-image cursor-pointer" src={props.product.image} alt="image" />
                        </div>
                    </div>
                </div>
            </div>
            <div className="md:col-span-7 col-span-12 bg-gray-300">
                <div className="text-2xl py-7">
                    Product type:  {props.product.type}
                </div>
                <div className="text-xl py-7">
                    {props.product.title}
                </div>
                <div className="text-sm text-gray-500 py-7">
                    {props.product.description}
                </div>
                <div className="text-xl py-7">
                    {props.product.location}
                </div>
            </div>
        </div>
    );
}

export default ProductDetails;