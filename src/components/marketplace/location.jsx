import React from 'react';

function Location(props) {

    return (
        <div className="grid grid-cols-12 gap-4 justify-items-start">
            <div className="col-span-12">
                {props.product.location}
            </div>
        </div>
    );
}

export default Location;