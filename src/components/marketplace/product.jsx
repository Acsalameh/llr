import React from 'react';
import Image from './image';
import Location from './location';
import Price from './price';
import Title from './title';



function Product(props) {

    return (
        <a href="#">
            <div className="h-full md:py-4 py-2 grid grid-cols-2 justify-items-start gap-4 shadow-xl duration-300 hover:scale-110 transform ">
                <div className="col-span-2 w-full md:px-4 px-2 ">
                    <Image product={props.product} />
                </div>
                <div className="col-span-2 md:px-4 px-2 w-full text-left">
                    <Title product={props.product} />
                </div>
                <div className="col-span-2 md:px-4 px-2 w-full">
                    <Price product={props.product} />
                </div>
                <div className="col-span-2 md:px-4 px-2 w-full">
                    <Location product={props.product} />
                </div>
            </div>
        </a>
    );
}

export default Product;