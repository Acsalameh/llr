import React from 'react';

function SearchBar(props) {

    return (

        <div className="grid grid-cols-5 mx-2 md:mx-10 pl-10 my-5 shadow-2xl rounded-lg relative">
            <div className="sm:col-span-3 col-span-2">
                <img src={props.searchbar.image} alt={props.searchbar.search} className="max-h-5 absolute left-3 pointer top-2/4 transform -translate-y-1/2" />
                <input className="outline-none bg-white py-5 w-full rounded-l-lg" type="text" placeholder={props.searchbar.placeholder} />
            </div>
            <div className="sm:col-span-1 col-span-2">
                <div className="bg-gray-200 w-full h-full md:px-8 px-2">
                    <select className="pointer bg-gray-200 outline-none w-full h-full" name="" id="">
                        {props.searchbar.options.map((option, i) => {
                            // selected logic is based on the iterator for now  
                            return <option value={option}>{option}</option>
                        })}
                    </select>
                </div>
            </div>
            <div className="col-span-1">
                <div className="pointer py-5 bg-blue-400 hover:bg-blue-800 duration-300 rounded-r-lg text-white">
                    {props.searchbar.search}
                </div>
            </div>
        </div>
    );
}

export default SearchBar;